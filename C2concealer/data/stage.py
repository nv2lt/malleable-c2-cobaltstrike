'''

##################################################################################
Data set for stage block including random tech-sounding names for the DLL
name + string replace and binary types for the http-stager-client.
##################################################################################

'''
#CUSTOMIZE THIS#
transform_names = ['DebugCommunications','SystemIntern','EncryptFull','CablePlatform','CommDebug','SafetyDebug','CommnicationsDebug','InternSystem','FullEncrypt','PlatformCable','DebugComm', 'DebugSafely','NetworkComm','NetworkInternals','NetworkEncryption','NetworkPlatform','NetworkDebug','DebugNetwork']

# These values are from explorer.exe
peclone_transform_name = {\
        "name":"explorer.exe",\
        "checksum":"0",\
        "compile_time":"04 Jan 2016 23:41:13",\
        "entry_point":"639232",\
        "image_size_x86":"4636672",\
        "image_size_x64":"4636672",\
        "rich_header":r"\x89\x04\x98\x55\xcd\x65\xf6\x06\xcd\x65\xf6\x06\xcd\x65\xf6\x06\xc4\x1d\x65\x06\x37\x65\xf6\x06\xd9\x0e\xf2\x07\xd2\x65\xf6\x06\xd9\x0e\xf5\x07\xce\x65\xf6\x06\xcd\x65\xf7\x06\x6c\x6c\xf6\x06\xd9\x0e\xf7\x07\xd2\x65\xf6\x06\xd9\x0e\xfb\x07\xab\x64\xf6\x06\xd9\x0e\xf3\x07\xee\x65\xf6\x06\xd9\x0e\x0b\x06\xcc\x65\xf6\x06\xd9\x0e\x09\x06\xcc\x65\xf6\x06\xd9\x0e\xf4\x07\xcc\x65\xf6\x06\x52\x69\x63\x68\xcd\x65\xf6\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",\
        "stringw":"explorer"}


#CUSTOMIZE THIS#
#**If you change this, you'll need to adjust the#
#elif statements in the consistencyCheck func in profile.py#
binary_types = [{'name':'jpg','content_type':'image/jpeg'},
{'name':'png','content_type':'image/png',},
{'name':'gif','content_type':'image/gif',},
{'name':'ico','content_type':'image/x-icon',},
{'name':'mp3','content_type':'audio/mpeg',},
]
