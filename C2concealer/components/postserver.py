import random
import base64
import uuid
import re
from ..data import reg_headers, params, transform, file_type_prepend


class postServer(object):

    '''
    Class for the http-post server component of the C2 profile.
    
    HTTP POST Section starts in 

    Profile options include:
    - Status
    - Connection 
    - Server
    - Cache-Control
    - Pragma
    - Content-Type

    You can always include more in here, like Content-Type and other headers

    '''

    def __init__(self):
        self.Status = None
        self.Connection = None
        self.Server = None
        self.Pragma = None
        self.Cache_Control = None
        self.headerList = ['Status','Connection','Server', 'Pragma', 'Cache_Control']
        self.output = []

    def randomizer(self, uris):
        '''
        Method to generate random postServer values.
        
        Will first check to see if value exists prior to generating new data.

        Output: postServer instance attributes are populated with random data.

        '''
        
        self.Status = str(200)
        self.Server = random.choice(reg_headers.server)
        self.Connection = 'keep-alive'
        self.Pragma = 'no-cache'
        self.Cache_Control = 'max-age=0, no-cache'
        
        # Build output
        self.output.append(random.choice(transform.transformations))
        self.output.append('base64url')
        
        uris = "/" + " /".join(uris)
        regexp = re.compile(r'\.js\s')
        if regexp.search(uris) is not None:
            self.output.append('prepend "' + random.choice(file_type_prepend.js) + '"')
            self.Content_Type = 'application/javascript; charset=utf-8'
        elif '.css' in uris:
            self.output.append('prepend "' + random.choice(file_type_prepend.css) + '"')
            self.Content_Type = 'text/css; charset=utf-8'
        else:
            self.output.append('prepend "' + random.choice(file_type_prepend.html) + '"')
            self.Content_Type = 'text/html; charset=utf-8'
        self.output.append('print')
 
    def printify(self):
        '''
        Method to print postServer attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the http-post server section of the profile.

        Example:
            server {
                output {
                    netbios;
                    base64;
                    print;
                }
            }

        } #included to close the entire http-post section
        
        '''

        profileString = ''
        profileString += '\t###############################################\n'
        profileString += '\t## HTTP SERVER Headers for HTTP POST Requests  #\n'
        profileString += '\t###############################################\n'
        profileString += '\tserver {\n'

        for header in self.headerList:
            headerValue = getattr(self, header)
            if headerValue == None:
                continue
            if "_" in header:
                header = header.replace("_","-")
            profileString += '\t\theader "' + header + '" "' + headerValue + '";\n'

        profileString += '\t\toutput {\n'
        for item in self.output:
            profileString += '\t\t\t' + item + ';\n'
        profileString += '\t\t}\n'

        profileString += '\t}\n'
        profileString += '}\n\n'
        
        return profileString
