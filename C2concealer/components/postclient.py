import random
import base64
import uuid
from ..data import urls, transform, reg_headers, params

class postClient(object):

    '''
    Class for the http-post client component of the C2 profile.

    + POST is used to send output to the teamserver
    + Can use HTTP GET or POST to send data
    + Note on using GET: Beacon will automatically chunk its responses (and use multiple requests) to fit the constraints of an HTTP GET-only channel.


    + Defaults:
        - uri "/activity"
        - Headers (Sample)
            + Accept: */*
            + Cookie: CN7uVizbjdUdzNShKoHQc1HdhBsB0XMCbWJGIRF27eYLDqc9Tnb220an8ZgFcFMXLARTWEGgsvWsAYe+bsf67HyISXgvTUpVJRSZeRYkhOTgr31/5xHiittfuu1QwcKdXopIE+yP8QmpyRq3DgsRB45PFEGcidrQn3/aK0MnXoM=
            + User-Agent Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1)

    + Guidelines:
        - Decide if you want to use HTTP GET or HTTP POST requests for this section
        - Add customize HTTP headers to the HTTP traffic of your campaign
        - Analyze sample HTTP traffic to use as a reference
    
    + Use HTTP POST for http-post section

    Profile options include:
    - uri   - Defaults: "/activity"
    - hostname
    - connection header
    - output (aka how to transform and store C2 client payload)
    - id (a session id to associate output with the right session)
    
    Note: 
    - The http-post level attribute "URI" is instantiated here.

    '''

    def __init__(self, name, host):
        self.uri = []
        self.verb = None
        self.Host = host
        self.Connection = None
        self.Accept = None
        self.Accept_Encoding = None
        self.Accept_Language = None
        self.Referer = None
        #self.Content_Type = None
        self.optionalHeaders = ['Accept', 'Accept_Encoding', 'Accept_Language', 'Referer']
        self.headerList = ['Host', 'Connection','Accept', 'Accept_Encoding', 'Accept_Language', 'Referer']
        self.output = []
        self.id = []
        self.name = name

    def randomizer(self):
            
        r'''
        Method to generate random postClient values.
        
        1. Choose 1-3 uris, optionally include a specific file type in uri.
        2. Choose 0-2 optional headers and generate random values.
        3. Create a session ID and store it in the cookie field
        4. Choose a type of form-upload and store output in there.

        NOTE: originally included multipart/form-data as a data output option, but
        it was consistently failing c2lint. code is below:

                boundary = "-"*random.randint(3,5) + str(uuid.uuid4())[:random.randint(5,9)].replace("-","")
                self.Content_Type = 'multipart/form-data; boundary=' + boundary
                self.output.append('prepend "Content-Disposition: form-data; name=\'' + \
                                                        random.choice(params.secret_params) + '\'\\r\\n\\r\\n"')
                self.output.append('prepend "--' + boundary + '\\r\\n"')
                self.output.append('append "--' + boundary + '--\\r\\n"')

        NOTE: removed the accept header since it was causing size issues + not sure it mattered that much

        Output: postClient instance attributes are populated with random data.

        '''
        uris = random.sample(urls.urls2, random.randint(1,3))
        if(random.randint(0,1)):
            for uri in uris:
                uri +=  "." + random.choice(urls.file_types)
        self.uri = uris

        self.Connection = 'Close'
        self.verb = "POST"

        optional = random.sample(self.optionalHeaders, random.randint(2,3))
        for header in optional:
            if header == 'Accept':
                self.Accept = random.choice(reg_headers.accept_header)
            elif header == 'Accept_Encoding':
                self.Accept_Encoding = random.choice(reg_headers.accept_encoding)
            elif header == 'Accept_Language':
                self.Accept_Language = random.choice(reg_headers.accept_language)
            elif header == 'Referer':
                self.Accept_Language = random.choice(reg_headers.referer)
 
        self.id.append('base64url')
        #CUSTOMIZE this prepend value#
        self.id.append('prepend "__jquerysessid_="')
        self.id.append('header "Cookie"')

        self.output.append(random.choice(transform.transformations))
        self.output.append('base64url')

        #form_type = random.randint(1,2)

        #if(form_type == 1):
        #    self.Content_Type = 'application/x-www-form-urlencoded'
        #    self.output.append('prepend "' + random.choice(params.common_params) + '="')
        #else:
        #    self.Content_Type = 'text/plain'
        self.output.append('print')

    def printify(self):

        '''
        Method to print postClient attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the http-post client section of the profile.

        Example:
            http-post "default" {
                set uri "/some/uri";
                    client {
                        header "Host" "somehost.com";
                        header "Connection" "close";
                        output {
                            base64;
                            print;
                        }
                       id {
                           netbios;
                           base64;
                           prepend="__session__id=";
                           header "Cookie";
                        }
                    }
            }
                    '''

        profileString = ''
        profileString += '########################################################\n'
        profileString += '## HTTP Headers for HTTP POST Requests (Server+Client) #\n'
        profileString += '########################################################\n'
        profileString += 'http-post "' + self.name + '" {\n'

        profileString += '\tset uri "/' + " /".join(self.uri) + '";\n' 
        profileString += '\tset verb "' + self.verb + '";\n\n'

        
        profileString += '\t###############################################\n'
        profileString += '\t## HTTP CLIENT Headers for HTTP POST Requests #\n'
        profileString += '\t###############################################\n'
        profileString += '\tclient {\n'

        for header in self.headerList:
            headerValue = getattr(self, header)
            if headerValue == None:
                continue
            if "_" in header:
                header = header.replace("_","-")
            profileString += '\t\theader "' + header + '" "' + headerValue + '";\n'

        profileString += '\t\toutput {\n'
        for item in self.output:
            profileString += '\t\t\t' + item + ';\n'
        profileString += '\t\t}\n'

        profileString += '\t\tid {\n'
        for item in self.id:
            profileString += '\t\t\t' + item + ';\n'
        profileString += '\t\t}\n'

        profileString += '\t}\n\n'
        
        return profileString
