import random
import datetime
from ..data import stage
import sys
#from ..helpers import chooseMEM_ALLOC


class stageBlock(object):

    '''
    Class for the stage component of the C2 profile.
    Deals with Memory Indicators

    Profile options include:
    - allocator     Default: VirtualAlloc, CS4.2-Set how Beacon's Reflective Loader allocates memory for the agent. Options are: HeapAlloc, MapViewOfFile, and VirtualAlloc
    - checksum      Default: - The CheckSum value in Beacon's PE header
    - userwx        Default: false - Ask ReflectiveLoader to use or avoid RWX permissions for Beacon DLL in memory
    - image_size_x64    Default:512000, SizeOfImage value in x64 Beacon's PE header
    - image_size_x86    Default:512000, SizeOfImage value in x86 Beacon's PE header 
    - sleep_mask    Default: false - Obfuscate Beacon (HTTP, SMB, TCP Beacons), in-memory, prior to sleeping (HTTP) or waiting for a new connection\data (SMB\TCP)
    - strrep DLL name and .dll file name; note that this replacement name is limited to 16 chars and dll to 6 chars
    - prepend (how many nops add before?)
    - cleanup       Default: false, Ask Beacon to attempt to free memory associated with the Reflective DLL package that initialized it.
    - stomppe       Default: true - Ask ReflectiveLoader to stomp MZ, PE, and e_lfanew values after it loads Beacon payload
    - obfuscate     Default: False - Obfuscate the Reflective DLL's import table, overwrite unused header content, and ask ReflectiveLoader to copy Beacon to new memory without its DLL headers. As of 4.2 CS now obfuscates .text section in rDLL package
    - compile_time  Default: 14 July 2009 8:14:00, The build time in Beacon's PE header
    - entry_point   Default: 92145, The EntryPoint value in Beacon's PE header
    - name          Default: beacon.x64.dll - The exported name of the Beacon DLL
    - magic_mz_x86  Default: MZRE, CS 4.2 - Override the first bytes (MZ header included) of Beacon's Reflective DLL. Valid x86 instructions are required. Follow instructions that change CPU state with instructions that undo the change.
    - magic_mz_x64  Default: MZAR,CS 4.2 - Same as magic_mz_x86; affects x64 DLL.
    - module_x86    Default: xpsservices.dll - Ask the x86 ReflectiveLoader to load the specified library and overwrite its space instead of allocating memory with VirtualAlloc.
    - module_x64    Default: xpsservices.dll - Same as module_x86; affects x64 loader
    - magic_pe      Default: PE - Override the PE character marker used by Beacon's Reflective Loader with another value.
    - rich_header   Default: N/A - Meta-information inserted by the compiler
    - smartinject   Default: false - CS 4.1 added to stage block - Use embedded function pointer hints to bootstrap Beacon agent without walking kernel32 EAT

    Lots of stuff here. I recommend reading up on it here:
    https://www.cobaltstrike.com/help-malleable-postex
    https://blog.cobaltstrike.com/2017/03/15/cobalt-strike-3-7-cat-meet-mouse/
    https://raw.githubusercontent.com/threatexpress/malleable-c2/master/jquery-c2.4.2.profile

    Guidelines:
    + Modify the indicators to minimize in memory indicators
    + Refer to:
        https://blog.cobaltstrike.com/2018/02/08/in-memory-evasion/
        https://www.youtube.com/playlist?list=PL9HO6M_MU2nc5Q31qd2CwpZ8J4KFMhgnK
        https://www.youtube.com/watch?v=AV4XjxYe4GM (Obfuscate and Sleep)
    
    '''

    def __init__(self, mem_alloc_option, usepeclone):
        if mem_alloc_option == "Module_Stomping":
            self.allocator = str("VirtualAlloc")
            self.module_x64 = str("netshell.dll")
            self.module_x86 = str("netshell.dll")
        else:
            self.allocator = str(mem_alloc_option)
            self.module_x64 = None
            self.module_x86 = None
        
        #self.allocator = None
        self.userwx = None
        self.magic_mz_x86 = None
        self.magic_mz_x64 = None
        self.magic_pe = None
        self.sleep_mask = None
        self.cleanup = None
        self.stomppe = None
        self.obfuscate = None
        self.smartinject = None
        self.checksum = None
        self.compile_time = None
        self.entry_point = None
        self.image_size_x86 = None
        self.image_size_x64 = None
        self.name = None
        self.rich_header = None
        self.stringw = None
        #self.allocator = str(mem_alloc_option)
        #self.module_x64 = None
        #self.module_x86 = None
        self.attrList = ['allocator', 'userwx', 'magic_mz_x86', 'magic_mz_x64', 'magic_pe', 'sleep_mask', 'cleanup', 'stomppe', 'obfuscate', 'smartinject', 'checksum', 'compile_time', 'entry_point', 'image_size_x86', 'image_size_x64', 'name', 'rich_header', 'module_x64', 'module_x86', 'stringw']
        self.prepend_count = None
        self.transform_name = None
        self.usepeclone = usepeclone


    def randomizer(self):
            
        '''
        Method to generate random stage values.
        
        -       checksum: 1-10 integer
        -       userwx: true for Heapalloc, false for other VirtualAlloc and mapreadfile
        -       image_size_x86: integer between 512,001 and 576,000
        -       image_size_x64: integer between 512,001 and 576,000
        -       sleep_mask: true
        -       cleanup: true
        -       stomppe: true
        -       obfuscate: true
        -       compile_time: a random day between 30 and 120 days ago
        -       entry_point: random int between 80000 and 90000
        -       name: random choice from /data/stage.py


        Output: stageBlock instance attributes are populated with random data.

        '''
        # Customize Memory Allocation
       
        if self.allocator == 'HeapAlloc':
            self.userwx = "true"
        else:
            self.userwx = "false"

        #CUSTOMIZE VALUES IN PE HEADER#
        self.magic_mz_x86 = "MZRE"
        self.magic_mz_x64 = "MZAR"
        self.magic_pe = "NO"

        self.sleep_mask = "true"
        self.cleanup = "true"
        self.stomppe = "true"
        self.obfuscate = "true"
        self.smartinject = "true"

        
        #CUSTOMIZE THE ENTRY POINT - Make the Beacon Reflective DLL look like something else in memory#
        # CAPTURE Values using peclone against a Windows 10 version of explorer.exe
        #self.checksum = "0"
        if self.usepeclone:
            try:
                self.name = stage.peclone_transform_name['name']
                self.checksum = stage.peclone_transform_name['checksum']
                self.compile_time = stage.peclone_transform_name['compile_time']
                self.entry_point = stage.peclone_transform_name['entry_point']
                self.image_size_x86 = stage.peclone_transform_name['image_size_x86']
                self.image_size_x64 = stage.peclone_transform_name['image_size_x64']
                self.rich_header = stage.peclone_transform_name['rich_header']
                self.stringw = stage.peclone_transform_name['stringw']
            except:
                print("\nNo peclone_transform_name variable defined in data/stage.py.\nDefine values in this variable using peclone tool and try again.")
                sys.exit("\nExiting")
        else:
            self.transform_name = str(random.choice(stage.transform_names))
            stage.peclone_transform_names = None
            self.checksum = str(random.randint(1,10))
            self.compile_time = (datetime.datetime.now()-datetime.timedelta(days=random.randint(30,120))).strftime("%d %b %Y %H:%M:%S")
            self.entry_point = str(random.randint(80000,90000))
            self.image_size_x86 = str(random.randint(512001,576000))
            self.image_size_x64 = str(random.randint(512001,576000))
            self.name = self.transform_name.lower() + ".dll"
            self.rich_header = r'\x3e\x98\xfe\x75\x7a\xf9\x90\x26\x7a\xf9\x90\x26\x7a\xf9\x90\x26\x73\x81\x03\x26\xfc\xf9\x90\x26\x17\xa4\x93\x27\x79\xf9\x90\x26\x7a\xf9\x91\x26\x83\xfd\x90\x26\x17\xa4\x91\x27\x65\xf9\x90\x26\x17\xa4\x95\x27\x77\xf9\x90\x26\x17\xa4\x94\x27\x6c\xf9\x90\x26\x17\xa4\x9e\x27\x56\xf8\x90\x26\x17\xa4\x6f\x26\x7b\xf9\x90\x26\x17\xa4\x92\x27\x7b\xf9\x90\x26\x52\x69\x63\x68\x7a\xf9\x90\x26\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
            self.stringw = "jQuery"

        self.prepend_count = random.randint(4,8)

    def printify(self):
            
        '''
        Method to print stage attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the stage section of the profile.

        Example:

        stage {
    
            set checksum "10";
            set userwx "false";
            set image_size_x86 "857033";
            set image_size_x64 "937448";
            set sleep_mask "true";

            transform-x86 {
            prepend "\x90\x90\x90\x90";
            strrep "ReflectiveLoader" "DebugMenu";
        }

        transform-x64 {
            prepend "\x90\x90\x90\x90";
            strrep "ReflectiveLoader" "DebugMenu";
        }

        }
        
        '''
        profileString = ''
        profileString += 'stage {\n\n'
        for item in self.attrList:
            attrValue = getattr(self, item)
            if (attrValue != None) and (item != "stringw"):
                if item == "module_x64":
                    profileString += '''\n\n\t## WARNING: Module stomping is enabled
\t# Cobalt Strike 3.11 also adds module stomping to Beacon's Reflective Loader. When enabled, Beacon's loader will shun VirtualAlloc and instead load a DLL into the current process and overwrite its memory.
\t# Set module_x86 to a favorite x86 DLL to module stomp with the x86 Beacon. The module_x64 option enables this for the x64 Beacon.
\t# While this is a powerful feature, caveats apply! If the library you load is not large enough to host Beacon, you will crash Beacon's process. If the current process loads the same library later (for whatever reason), you will crash Beacon's process. Choose carefully.
\t# By default, Beacon's loader allocates memory with VirtualAlloc. Module stomping is an alternative to this. Set module_x86 to a DLL that is about twice as large as the Beacon payload itself. Beacon's x86 loader will load the specified DLL, find its location in memory, and overwrite it. This is a way to situate Beacon in memory that Windows associates with a file on disk. It's important that the DLL you choose is not needed by the applications you intend to reside in. The module_x64 option is the same story, but it affects the x64 Beacon.
\t# If something goes wrong with module stomping, beacon will fallback to VirtualAlloc
\t# Details can be found in the In-memory Evasion video series. https://youtu.be/uWVH9l2GMw4'''
                    profileString += '\n'

                profileString += '\tset ' + item + ' "' + attrValue + '";\n'
        profileString += '\n'

        profileString += '\ttransform-x86 {\n'
        profileString += '\t\tprepend "' + '\\x90'*self.prepend_count + '";\n'
        #profileString += '\t\tstrrep "ReflectiveLoader" "' + self.transform_name[:16] + '";\n'
        profileString += '\t\tstrrep "ReflectiveLoader" "' + "" + '";\n'
        profileString += '\t\tstrrep "This program cannot be run in DOS mode" "' + "" + '";\n'
        profileString += '\t\tstrrep "beacon.dll" "' + "" + '";\n'
        profileString += '\t}\n\n'

        profileString += '\ttransform-x64 {\n'
        profileString += '\t\tprepend "' + '\\x90'*self.prepend_count + '";\n'
        #profileString += '\t\tstrrep "ReflectiveLoader" "' + self.transform_name[:16] + '";\n'
        profileString += '\t\tstrrep "ReflectiveLoader" "' + "" + '";\n'
        profileString += '\t\tstrrep "This program cannot be run in DOS mode" "' + "" + '";\n'
        profileString += '\t\tstrrep "beacon.x64.dll" "' + "" + '";\n'
        profileString += '\t}\n\n'
        profileString += '\tstringw "' + self.stringw + '";\n'

        profileString += '}\n\n'
        return profileString 
