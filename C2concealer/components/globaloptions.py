import uuid
import random
from ..data import reg_headers

class globalOptions(object):

    '''
    Class for the global options section of the profile. 

    Profile options include:
    - sample_name
    - sleeptime
    - jitter
    - tcp_port          Defaults: 4444
    - tcp_frame_header  Added in 4.1, prepend header to TCP Beacon messages/frames-Defaults: N/A
                        Used to push back on static detections that target the structure and content of named pipe Beacon messages 
    - user_agent
    - data_jitter   Defaults:0 - Append random-length string (up to data_jitter value) to http-get and http-post server output.
    - host_stage    Default: true - Host payload for staging over HTTP, HTTPS, or DNS. Required by stagers.

    Guidelines:
        - OPSEC WARNING!!!!! The default port is 4444. This is bad. You can change dynamicaly but the port set in the profile will always be used first before switching to the dynamic port.
        - Use a port other that default. Choose something not is use.
        - Use a port greater than 1024 is generally a good idea

    For more details visit:
    https://www.cobaltstrike.com/help-malleable-c2

    '''

    def __init__(self):
        self.sample_name = None
        self.sleeptime = None
        self.jitter = None
        self.tcp_port = None
        self.useragent = None
        self.tcp_frame_header = None
        self.data_jitter = None
        self.host_stage = None

    def randomizer(self):

        '''
        Method to generate random globalOptions values.

        1. sample_name set to first 8 chars from the uuid.uuid4() func
        2. sleeptime is set to a random integer between 55000ms and 65000ms
        3. jitter is set to an odd number between 37 and 45 (%)
        4. tcp_port is TCP Beacon Port. Randomly selected between 1024-10000 (not 4443-4446)
        5. tcp_frame_port is the size of the prepend header to TCP Beacon Messages. 
        6. Default User-Agent for HTTP comms. Randomly selected. CS < 4.2 aprox 128 max characters, CS 4.2+ max 255 characters 

        Output: globalOptions instance attributes are populated with random data.

        '''

        self.sample_name = str(uuid.uuid4())[:8]
        self.sleeptime = str(random.randint(55000,65000))
        self.jitter = str(random.randrange(37,45,2))
        self.tcp_port = str(random.choice((list(range(1024,4442))+list(range(4447,10000)))))
        self.useragent = random.choice(reg_headers.user_agent)

        #Static
        self.tcp_frame_header = "\\x80"
        self.data_jitter = '100'
        self.host_stage = 'false'

    def printify(self):

         '''
         Method to print global options attributes to string formatted to Cobalt Strike recs.

         Output: returns a string with attribute values, headers and appropriate 
         indentation/line-breaks formatted for the global options section of the profile.

         Example:

         set sample_name "UUID-UUID";
         set sleeptime "60";
         set jitter "50";
         set tcp_port 34857;
         set useragent "Mozilla 5.0";

         '''

         profileString = ''
         for attr, value in self.__dict__.items():
                 profileString += 'set ' + attr + ' "' + value + '";\n'
         profileString += '\n'
         return profileString
