import random
from ..data import ssh

class sshOptions(object):

    '''
    Class for the dns options section of the profile.
    Peer-to-peer SSH pseudo-Beacon for lateral movement

    Profile options include:
    - ssh_banner    Added in 4.1, changes ssh client SSH banner. Default in 4.2: Cobalt Strike 4.2
    - ssh_pipename  Name of pipe for SSH. Default: postex_ssh_#### - # replaced with random hex
    
    Learn more about these values here:
    https://www.cobaltstrike.com/help-malleable-c2
    '''

    def __init__(self):
        self.ssh_banner = None
        self.ssh_pipename = None

    def randomizer(self):

        '''
        Method to generate random dnsOptions values.

        1. ssh_banner = pull a random ssh banner value from list in /data/ssh.py
        2. ssh_pipename = pull a random ssh pipename from list in /data/ssh.py and add #### at the end

        '''

        self.ssh_banner = str(random.choice(ssh.banners))
        self.ssh_pipename = str(random.choice(ssh.pipenames)) + "####"


    def printify(self):

        '''
        Method to print dns options attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the dns options section of the profile.

        Example:

        set ssh_banner "OpenSSH_7.4 Debian (protocol 2.0)";
        set ssh_pipename "wkssvc##";

        '''

        profileString = ''
        for attr, value in self.__dict__.items():
            profileString += 'set ' + attr + ' "' + value + '";\n'
        profileString += '\n'
        return profileString 
