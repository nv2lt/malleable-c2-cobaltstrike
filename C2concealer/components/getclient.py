import random
import base64
import uuid
from ..data import urls, transform, reg_headers, params

class getClient(object):

    '''
    Class for the http-get client component of the C2 profile.
    
    GET is used to poll teamserver for tasks

    Profile options include:
    - uri   -Defaults: "/activity"
    - host
    - connection
    - Accept
    - Accept-Encoding
    - Accept-Language
    - metadata (aka how to transform and store C2 client payload)
    - http-get parameters

    ##    Headers (Sample)
    ##      Accept: */*
    ##      Cookie: CN7uVizbjdUdzNShKoHQc1HdhBsB0XMCbWJGIRF27eYLDqc9Tnb220an8ZgFcFMXLARTWEGgsvWsAYe+bsf67HyISXgvTUpVJRSZeRYkhOTgr31/5xHiittfuu1QwcKdXopIE+yP8QmpyRq3DgsRB45PFEGcidrQn3/aK0MnXoM=
    ##      User-Agent Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1)

    + Guidelines:
        - Add customize HTTP headers to the HTTP traffic of your campaign
        - Analyze sample HTTP traffic to use as a reference
        - Multiple URIs can be added. Beacon will randomly pick from these.
        - Use spaces as a URI seperator
    
    '''

    def __init__(self, name, host):
        self.uri = []
        self.verb = 'GET'
        self.Host = host
        self.Accept = None
        self.Accept_Encoding = None
        self.Accept_Language = None
        self.Connection = 'close'
        self.Referer = None
        self.User_Agent = None
        self.optionalHeaders = ['Accept', 'Accept_Encoding', 'Accept_Language', 'Referer']
        self.headerList = ['Host', 'Connection','Accept', 'Accept_Encoding', 'Accept_Language', 'Referer', 'User_Agent']
        self.metadata = []
        self.parameters = {}
        self.name = name

    def randomizer(self):
            
        '''
        Method to generate random getClient values.
        
        1. For uri, grab a url path from /data/urls
        2. Choose 0 - 2 headers from among Accept, Accept-Encoding, Accept-Language
        3. Randomly choose 1 transformation for metadata, base64 encode it, then append a cookie_prefix from /data/reg_headers
        and then throw into the Cookie header.
        4. Generate random params

        Output: getClient instance attributes are populated with random data.

        '''

        uris = random.sample(urls.urls1, random.randint(1,3))
        new_uris = []
        if(random.randint(0,1)):
            file_type = random.choice(urls.file_types)
            for uri in uris:
                new_uris.append(uri + "." + file_type)
        else:
            new_uris = uris
        self.uri = new_uris              

        #optional = random.sample(self.optionalHeaders, random.randint(0,3))
        optional = random.sample(self.optionalHeaders, random.randint(2,3))
        for header in optional:
            if header == 'Accept':
                self.Accept = random.choice(reg_headers.accept_header)
            elif header == 'Accept_Encoding':
                self.Accept_Encoding = random.choice(reg_headers.accept_encoding)
            elif header == 'Accept_Language':
                self.Accept_Language = random.choice(reg_headers.accept_language)
            elif header == 'Referer':
                self.Accept_Language = random.choice(reg_headers.referer)
        
        self.metadata.append(random.choice(transform.transformations))
        self.metadata.append('prepend "' + random.choice(reg_headers.cookie_prefixes) + '="')
        self.metadata.append('base64url')
        self.metadata.append('header "Cookie"')

        common_params = random.sample(params.common_params,random.randint(0,1))
        for param in common_params:
            #CUSTOMIZE THIS LIST OF PARAM VALUES (doesn't have any impact on performance)#
            paramVal = random.choice(['true','false',])
            self.parameters[param] = paramVal


    def adduseragent(self, newname, useragent):
        self.name = str(newname)
        self.User_Agent = str(useragent)


    def printify(self):

        '''
        Method to print getClient attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the http-get client section of the profile.

        Example:

                http-get "default" {
        
                        set uri "/some/uri";

                        client {
        
                                header "Host" "somehost.com";
                                header "Connection" "close";

                                metadata {
        
                                        base64;
                                        header "Cookie";

                                }

                                parameter "apple" "green";

                        }
                }
        
        '''

        profileString = ''
        profileString += '########################################################\n'
        profileString += '## HTTP Headers for HTTP GET Requests (Server+Client)  #\n'
        profileString += '########################################################\n'
        profileString += 'http-get "' + self.name + '" {\n'
        profileString += '\tset uri "/' + " /".join(self.uri) + '";\n'
        profileString += '\tset verb "' + self.verb + '";\n\n'

        profileString += '\t###############################################\n'
        profileString += '\t## HTTP CLIENT Headers for HTTP GET Requests  #\n'
        profileString += '\t###############################################\n'
        profileString += '\tclient {\n'

        for header in self.headerList:
            headerValue = getattr(self, header)
            if headerValue == None:
                continue
            if "_" in header:
                header = header.replace("_","-")
            profileString += '\t\theader "' + header + '" "' + headerValue + '";\n'

        profileString += '\t\tmetadata {\n'
        for item in self.metadata:
            profileString += '\t\t\t' + item + ';\n'
        profileString += '\t\t}\n'

        for param, value in self.parameters.items():
            profileString += '\t\tparameter "' + param + '" "' + value + '";\n'

        profileString += '\t}\n\n'
        
        return profileString
