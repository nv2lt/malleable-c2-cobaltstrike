import random
from ..data import reg_headers

class httpConfig(object):

    '''
    Class for the http config component of the C2 profile.
    The http-config block has influence over all HTTP responses served by Cobalt Strike’s web server. Here, you may specify additional HTTP headers and the HTTP header order.
                                                       
    Profile options include:
    - headers   "Comma separated list of headers" - The set headers option specifies the order these HTTP headers are delivered in an HTTP response. Any headers not in this list are added to the end.

    -  headers  "headername" "header alue - The header keyword adds a header value to each of Cobalt Strike's HTTP responses. If the header value is already defined in a response, this value is ignored. Example:
        + Server "Apache" (this can be overridden in sub-components, in fact, the stager will
      almost always have a varied server to avoid blue teams linking stager to redirector)

    - Trust_X_Forwarded_for     "true" - Add this header to determine remote address of a request.

    From CS documentation (https://www.cobaltstrike.com/help-malleable-c2):
    "The set trust_x_forwarded_for option decides if Cobalt Strike uses the X-Forwarded-For 
    HTTP header to determine the remote address of a request. Use this option if your 
    Cobalt Strike server is behind an HTTP redirector."

    - Guidelines:
        + Use this section in addition to the "server" secion in http-get and http-post to further define the HTTP headers
    '''

    def __init__(self):
        self.headers = None
        self.Server = None
        self.Keep_Alive = None
        self.Connection = None
        self.trust_x_forwarded_for = None
        self.attrList = ['headers', 'Server', 'Keep_Alive', 'Connection', 'trust_x_forwarded_for']

    def randomizer(self):
        
        '''
        Method to generate random http config values.
        
        1. Choose random server value from /data/reg_headers

        Output: httpConfig instance attributes are populated with random data.

        '''
        
        self.headers = "Date, Server, Content-Length, Keep-Alive, Connection, Content-Type"
        self.Server = random.choice(reg_headers.server)
        self.trust_x_forwarded_for = "true"
        self.Keep_Alive = "100"
        self.Connection = "Keep-Alive"; 
        

    def printify(self):
            
        '''
        Method to print httpConfig attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the http config section of the profile.

        Example:

        http-config {
    
                header "Server" "Apache";

        }
        
        '''
        profileString = ''
        profileString += '################################################################\n'
        profileString += '#HTTP Headers for all HTTP Responses of CobaltStrike Web Server#\n'
        profileString += '################################################################\n'
        profileString += 'http-config {\n'
        for attr in self.attrList:
            attrValue = getattr(self, attr)
            if attr == 'headers' or attr == 'trust_x_forwarded_for':
                profileString += '\tset ' + attr + ' "' + attrValue + '";\n'
            else:
                if "_" in attr:
                    attr = attr.replace('_','-')
                profileString += '\theader "' + attr + '" "' + attrValue + '";\n'
        profileString += '}\n\n'
        return profileString
