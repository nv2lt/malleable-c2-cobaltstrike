import random

class processInject(object):

    '''
    Class for the process inject component of the C2 profile.

    Profile options include:
    - allocator     Default:VirtualAllocEx - The preferred method to allocate memory in the remote process. Specify VirtualAllocEx or NtMapViewOfSection. The NtMapViewOfSection option is for same-architecture injection only. VirtualAllocEx is always used for cross-arch memory allocations.
    - min_alloc     Default:4096 - Minimum amount of memory to request for injected content.
    - userwx        Default: false - Use RWX as final permissions for injected content. Alternative is RX.
    - startrwx      Default: False - Use RWX as initial permissions for injected content. Alternative is RW. 
    - transform-x86 nop prepend count
    - transform-x64 nop prepend count
    - execute is hardcoded, see below in printify()

    For more information:
    https://www.cobaltstrike.com/help-malleable-postex
    https://blog.cobaltstrike.com/2019/08/21/cobalt-strikes-process-injection-the-details/
    
    Use the transform-x86-x64 to pad content injected by Beacon
    Use the execute block to control use of Beacon's process injection techniques
    
    + Guidelines:
        - Modify the indicators to minimize in memory indicators
        - https://www.cobaltstrike.com/help-malleable-c2#processinject
        - https://blog.cobaltstrike.com/2019/08/21/cobalt-strikes-process-injection-the-details/

    '''

    def __init__(self):
        self.allocator = None
        self.min_alloc = None
        self.userwx = None
        self.startrwx = None
            

    def randomizer(self):
            
        '''
        Method to generate random process inject values.
        
        1. All static except min_alloc which is an integer larger than 4096, max 32000, not 16384

        Output: processInject instance attributes are populated with random data.

        '''
        # set a remote memory allocation technique: VirtualAllocEx|NtMapViewOfSection
        self.allocator = "NtMapViewOfSection"

        # Minimium memory allocation size when injecting content
        self.min_alloc = str(random.choice((list(range(4096,16383))+list(range(16385,32000)))))

        # Set memory permissions as permissions as initial=RWX, final=RX
        self.userwx = "false"
        self.startrwx = "false"

    def printify(self):
            
        r'''
        Method to print process inject attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the process inject section of the profile.

        Example:

        process-inject {

            set allocator "virtualallocex";             
            set min_alloc "10374";
            set userwx "false"; 
            set startrwx "false";

            transform-x86 {
                        prepend "\x90\x90\x90";
            }
            transform-x64 {
                        prepend "\x90\x90\x90";
            }
           
            execute {
                CreateThread;
                RtlCreateUserThread;
                CreateRemoteThread;
            }
        }
        
        '''

        prepend_count = random.randint(3,8)

        profileString = ''
        profileString += 'process-inject {\n'
        for attr, value in self.__dict__.items():
            profileString += '\tset ' + attr + ' "' + value + '";\n'
        profileString += '\n'

        profileString += "\t# Transform injected content to avoid signature detection of first few bytes. Only supports prepend and append.\n"
        profileString += '\ttransform-x86 {\n'
        profileString += '\t\tprepend "' + "\\x90"*prepend_count+ '";\n'
        profileString += '\t\tappend "' + "\\x90"*prepend_count+ '";\n'
        profileString += '\t}\n'
        profileString += '\ttransform-x64 {\n'
        profileString += '\t\tprepend "' + "\\x90"*prepend_count + '";\n'
        profileString += '\t\tappend "' + "\\x90"*prepend_count + '";\n'
        profileString += '\t}\n'

        profileString += '\n\n'

        ##Hardcoded Execute block
        profileString += '''\
    #The execute block controls the methods Beacon will use when it needs to inject code into a process. Beacon examines each option in the execute block, determines if the option is usable for the current context, tries the method when it is usable, and moves on to the next option if code execution did not happen. The execute options include:
    #
    # Na                    x86->x64    x64-x86     Notes
    #####################################################################
    # CrThread                                      Current Process only
    # CrRemoteThread                       Yes      No cross-session
    # NteApcThread                                  
    # NtPCThread-s                                  This is the "Early Bird" injection technique. Suspended processes (e.g., post-ex jobs) only.
    # RtateUserThread           Yes        Yes      Risky on XP-era targets; uses RWX shellcode for x86->x64 injection.
    # SeeadContext                         Yes      Suspended processes (e.g. post-ex jobs only) \n'''

        profileString += '\texecute {\n'
        profileString += '\t\tCreateThread;\n'
        profileString += '\t\tNtQueueApcThread-s;\n'
        profileString += '\t\tCreateRemoteThread;\n'
        profileString += '\t\tRtlCreateUserThread;\n'
        profileString += '\t}\n'
        ##End execute block

        profileString += '}\n\n'
        return profileString
