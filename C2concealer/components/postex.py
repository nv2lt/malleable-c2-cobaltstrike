import random
from ..data import post_ex

class postEx(object):

    '''
    Class for the post-ex block component of the C2 profile.

    Profile options include:
    - spawnto-x86   Defaults: %windir%\\syswow64\\rundll32.exe
    - spawnto-x64   Defaults: %windir%\\sysnative\\rundll32.exe
    - obfuscate     Defaults: false
    - pipename      Defaults: postex####, windows\\pipe_##, CS4.2 - Change the named pipe names used, by post-ex DLLs, to send output back to Beacon. This option accepts a comma-separated list of pipenames. Cobalt Strike will select a random pipe name from this option when it sets up a post-exploitation job. Each # in the pipename is replaced with a valid hex character as well.

    - smartinject   Defaults: false, CS 3.14 added to postex block - Directs Beacon to embed key function pointers, like GetProcAddress and LoadLibrary, into its same-architecture post-ex DLLs.

    - amsi-disable  Defaults: false, CS 3.13 - Directs powerpick, execute-assembly, and psinject to patch the AmsiScanBuffer function before loading .NET or PowerShell code. This limits the Antimalware Scan Interface visibility into these capabilities.

    - keylogger     Defaults: GetAsyncKeyState, CS 4.2, The GetAsyncKeyState option (default) uses the GetAsyncKeyState API to observe keystrokes. The SetWindowsHookEx option uses SetWindowsHookEx to observe keystrokes.
    - threadhint    CS 4.2 - allows multi-threaded post-ex DLLs to spawn threads with a spoofed start address. Specify the thread hint as "module!function+0x##" to specify the start address to spoof. The optional 0x## part is an offset added to the start address. 

    Guidelines:
    + spawnto can only be 63 chars
    + OPSEC WARNING!!!! The spawnto in this example will contain identifiable command line strings
    + sysnative for x64 and syswow64 for x86
        - Example x64 : C:\\Windows\\sysnative\\w32tm.exe
        - Example x86 : C:\\Windows\\syswow64\\w32tm.exe
    + The binary doesnt do anything wierd (protected binary, etc)
    + !! Don't use these !! 
        - "csrss.exe","logoff.exe","rdpinit.exe","bootim.exe","smss.exe","userinit.exe","sppsvc.exe"
    + A binary that executes without the UAC
        - 64 bit for x64
        - 32 bit for x86
    + You can add command line parameters to blend
        - set spawnto_x86 "%windir%\\syswow64\\svchost.exe -k netsvcs";
        - set spawnto_x64 "%windir%\\sysnative\\svchost.exe -k netsvcs";
        - Note: svchost.exe may look weird as the parent process

    + The obfuscate option scrambles the content of the post-ex DLLs and settles the post-ex capability into memory in a more OPSEC-safe way. It’s very similar to the obfuscate and userwx options available for Beacon via the stage block.
    
    + The amsi_disable option directs powerpick, execute-assembly, and psinject to patch the AmsiScanBuffer function before loading .NET or PowerShell code. This limits the Antimalware Scan Interface visibility into these capabilities.
    
    + The smartinject option directs Beacon to embed key function pointers, like GetProcAddress and LoadLibrary, into its same-architecture post-ex DLLs. This allows post-ex DLLs to bootstrap themselves in a new process without shellcode-like behavior that is detected and mitigated by watching memory accesses to the PEB and kernel32.dll

    For more information:
    https://www.cobaltstrike.com/help-malleable-postex
    https://github.com/threatexpress/malleable-c2/blob/master/jquery-c2.4.0.profile

    '''

    def __init__(self):
        self.spawnto_x86 = None
        self.spawnto_x64 = None
        self.obfuscate = None
        self.smartinject = None
        self.amsi_disable = None
        self.pipename = None
        self.keylogger = 'GetAsyncKeyState'

    def randomizer(self):
            
        '''
        Method to generate random post-ex block values.
        
        1. Choose a random exe from list in /data/post-ex.py for spawnto attrs

        Output: postEx instance attributes are populated with random data.

        '''
        proc = str(random.choice(post_ex.spawn_processes))
        self.spawnto_x86 = "%windir%\\\\syswow64\\\\" + proc
        self.spawnto_x64 = "%windir%\\\\sysnative\\\\" + proc
        self.pipename = str(random.choice(post_ex.pipenames))
        print(self.pipename)
        
        #Static
        self.obfuscate = "true"
        self.smartinject = "true"
        self.amsi_disable = "true"


    def printify(self):
            
        '''
        Method to print postEx attributes to string formatted to Cobalt Strike recs.

        Output: returns a string with attribute values, headers and appropriate 
        indentation/line-breaks formatted for the post-ex block section of the profile.

        Example:

        post-ex {

            set spawnto_x86 "%windir%\\syswow64\\rundll32.exe";
            set spawnto_x64 "%windir%\\sysnative\\rundll32.exe";
            set obfuscate "true";
            set smartinject "true";
            set amsi_disable "true";

        }
        
        '''
        profileString = ''
        profileString += 'post-ex {\n'
        for attr, value in self.__dict__.items():
                profileString += '\tset ' + attr + ' "' + value + '";\n'
        profileString += '\t#set threadhint "module!function+0x##"' + '\n'
        profileString += '}\n\n'
        return profileString 
