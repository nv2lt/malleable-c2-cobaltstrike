# C2concealer - Fork

This is a fork of the C2concealer is a command line tool that generates randomized C2 malleable profiles for use in Cobalt Strike.
The main development work (original code) can be found here [ForthNorthSecurity/C2concealer](https://github.com/FortyNorthSecurity/C2concealer)
This fork adds:
  + Support for CS 4.2
  + Choices based on [threatexpress/malleable-c2](https://github.com/threatexpress/malleable-c2/blob/master/jquery-c2.4.2.profile)
  + Options to take advantage of the excellent work of [EncodeGroup/AggressiveProxy](https://github.com/EncodeGroup/AggressiveProxy)

## Installation 
  + As defined in the original code from [ForthNorthSecurity/C2concealer](https://github.com/FortyNorthSecurity/C2concealer) 
```bash
chmod u+x install.sh
./install.sh
```

## Example Usage
### Example 1
```bash
Usage:
	$ C2concealer --hostname google.com --variant 3

Flags:
	
	(optional)
	--hostname 
		The hostname used in HTTP client and server side settings. Default is None.
	--variant 
		An integer defining the number of HTTP client/server variants to generate. 
		Recommend between 1 and 5. Max 10.
```
### Example 2
```bash
Usage:
	$ C2concealer --usepeclone
```

+ Flag (optional):
	- `- usepeclone` 
    * Affects the stage block of the profile. It will use the values found in data/stage.py under the variable `peclone_transform_name`.   
    * Use peclone tool of cobaltstrike to get the appropriate values of a binary you want to resemble and manually change the values of `peclone_transform_name`. 
    * In the provided example values were used based on windows 10 file explorer `explorer.exe`.

### Example 3
```bash
Usage:
	$ C2concealer --aggressiveproxy
	$ C2concealer --usepeclone --aggressiveproxy
```
+ Flag (optional):
  - `-aggressiveproxy`
    * This flag created 3 variants with names `chrome`, `firefox`, `edge` and the appropriate values in order to take advantage of the excellent work of [EncodeGroup/AggrssiveProxy](https://github.com/EncodeGroup/AggressiveProxy)

## Example Console Output

```bash
root@kali:~# C2concealer --usepeclone --aggressiveproxy --hostname google.com
[i] Found c2lint in the /opt/cobaltstrike/ directory.

Choose an SSL option:
1. Self-signed SSL cert (just input a few details)
2. LetsEncrypt SSL cert (requies a temporary A record for the relevant domain to be pointed to this machine)
3. Existing keystore
4. No SSL

[?] Option [Enter Number]: 4
Choose an option for memory allocation:
1. VirtualAlloc
2. HeapAlloc
3. MapViewOfFile
4. Module Stomping. By default 'netshell.dll' will be used. Change it manually later if you wish

[?] Option [Enter Number]: 2
```
> Tip: Always use an SSL certificate. Preferably a cert from LetsEncrypt or similar.

> Tip: HTTP Variants allow you to select different IOCs for http traffic on different beacons. Recommend a value of at least 1. 

> Tip: The option for memory allocation choice is also an addition of this fork. Based on the choice for memory allocation flag for rwx will be also affected accordingly. If Module Stomping cannot be implemented cobaltstrike will use VirtualAlloc so the profile is build based on this behaviour.


 ## Shoutouts

+ Big shoutout to Raphael Mudge for constantly improving on the malleable profile feature set and the documentation to learn about it. Also, huge thanks to @killswitch-GUI for his script that automates LetsEncrypt cert generation for CS team servers. Finally, two blog posts that made life so much easier: @bluescreenofjeff's post (https://bluescreenofjeff.com/2017-01-24-how-to-write-malleable-c2-profiles-for-cobalt-strike/) and Joe Vest's post (https://posts.specterops.io/a-deep-dive-into-cobalt-strike-malleable-c2-6660e33b0e0b).

+ Need to emphasize that this is a fork/customization of the initial work made by [ForthNorthSecurity/C2concealer](https://github.com/FortyNorthSecurity/C2concealer). Check their project for more information.

+ Be sure to check out the excellent work made by [EncodeGroup](https://github.com/EncodeGroup/AggressiveProxy)
+ Huge thanks also for the work made under project: [threatexpress/malleable-c2](https://github.com/threatexpress/malleable-c2/blob/master/jquery-c2.4.2.profile)
 

